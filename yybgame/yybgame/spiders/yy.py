# -*- coding: utf-8 -*-
import scrapy
from scrapy import Spider,Selector
import time
from scrapy.http import Request
import re
import json
from yybgame.items import YybgameItem

class YybSpider(Spider):
    name = "yybgame"
    allowed_domains = ['android.myapp.com']
    start_urls = ["http://android.myapp.com/myapp/category.htm?orgame=1"] ###103video  122shopping  101music 
    url = "http://android.myapp.com/myapp/"
    def parse(self,response):
        sel = Selector(response)
        games = sel.xpath('//ul[@class="app-list clearfix"]/li/div')
        for each in games:
            href = each.xpath('a/@href').extract()[0]
            gameUrl = re.sub('(.*)htm',self.url+'app/comment.htm',href)+'&contextData'
            yield Request(gameUrl, callback = self.parse_item)
    def parse_item(self,response):
        jsDict = json.loads(response.body)
        jsData = jsDict['obj']
        try:
            comments = jsData['commentDetails']
        except Exception,e:
            pass
        for each in comments:
            item = YybgameItem()
            item['app'] = re.findall('=com.(.*)&',response.url)[0]
            item['name'] = each['nickName']
            item['star'] = each['score']
            content = each['content']
            if content:
                item['content'] = content
            else:
                item['cntent'] = ''
            item['time'] = time.strftime("%Y-%m-%d %H:%M:%S",time.localtime(each['createdTime']))
            
            yield item
        id = re.search('id=(\d+)', response.body, re.S).group(1)
        if id:
            nextLink = re.sub('Data(.*)', 'Data=id='+id, response.url)
            yield Request(nextLink, callback = self.parse_item)
